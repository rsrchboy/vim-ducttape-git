package VIMx::autoload::ducttape::git::head;

use v5.10;
use strict;
use warnings;

use VIMx::Symbiont;
use VIMx::ducttape::git;

function subject => sub {

    my $head = try {
        bufrepo->head;
    }
    catch {
        return "!no such $+{ref}"
            if $_ =~ /reference '(?<ref>[^']+)' not found/;
        die $_;
    };

    return $head
        unless ref $head;

    return $head->peel('commit')->summary;
};

function target_subject => sub { bufrepo->head->target->summary         };

!!42;
__END__
